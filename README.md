# README #

**General**
Although the assignment was very specific I treated the code as it was a part of much larger code base.
That is why some parts may seem unnecessary at first (for example: having an abstract caching class even though there’s only one caching type) but they do make sense when thinking about the assignment as a small part in a much larger app.

**Architecture**
I choose MVP architecture.
It's simple and powerful and it helps me separate the different layers of my app.
This allows me to write a cleaner, more maintainable and more "testable" code.

**REST calls**
I used Retrofit2 as my rest client.
It’s powerful and backed by some of the best Android dev’s. I can trust it completely.
If i’ll ever need to customize my "networking needs” I can always customize the underling OkHttp client.

**Cities DB**
As a core feature, I wanted the search mechanism to be as fast and user friendly as possible.
That is why I decided to create a local DB to store all the available cities.
Wrapped inside an AsyncTaskLoader for async query it is fast enough to allow searching while typing.
With that said, It’s been a while since I worked directly with local SQLite DB (and not a DAO or nosql solutions) 
so I guess the implementation is probably lacking. I had fun though.

**Testing**
I wrote some unit tests, mostly around the cities db functionality but I believe the code itself is pretty testable
and I think this is the most challenging part of writing any kind of test.
Sadly, Due to time constraints,This is in no way a full coverage.

**UI/UX**
Nothing really fancy here. The app should be instantly familiar to any Android user.
I would like to note that, due to time constraints, the MainActivity does not support orientation changes.

**Misc**
I hope this readme file covers everything it should. I'll be more than happy to discuss anything I might have missed. Thanks :)