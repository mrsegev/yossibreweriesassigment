package com.yossisegev.yossibrewerydemo;

import com.yossisegev.yossibrewerydemo.view.MvpView;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
/**
 * Created by Yossi Segev on 14/12/2016.
 */

public class PresenterTest {

    private MockPresenter mockPresenter;

    @Before
    public void setup() {
        mockPresenter = new MockPresenter();
    }

    @Test
    public void testViewIsAttached() {
        MockView mockView = new MockView() {};
        mockPresenter.attachView(mockView);
        assertTrue("View is not attached", mockPresenter.attached());
        mockPresenter.detachView();
        assertFalse("View did not detach", mockPresenter.attached());
    }

}
