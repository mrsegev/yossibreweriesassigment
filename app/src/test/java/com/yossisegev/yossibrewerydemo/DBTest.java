package com.yossisegev.yossibrewerydemo;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import static org.junit.Assert.*;

import com.yossisegev.yossibrewerydemo.entities.City;
import com.yossisegev.yossibrewerydemo.model.db.CityDB;
import com.yossisegev.yossibrewerydemo.model.db.CityDBHelper;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import java.util.List;

/**
 * Created by Yossi Segev on 13/12/2016.
 */

@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class, sdk = 21, manifest = "/src/main/AndroidManifest.xml")
public class DBTest {

    private Context context;
    private CityDBHelper cityDBHelper;
    private SQLiteDatabase db;

    @Before
    public void setup() {
        context = RuntimeEnvironment.application;
        cityDBHelper = new CityDBHelper(context);
    }

    @Test
    public void testDBCreated() {
        db = cityDBHelper.getReadableDatabase();
        assertTrue("DB is not open", db.isOpen());
        Cursor cursor = db.query(CityDB.CITIES_TABLE, null, null, null, null, null, null);
        assertNotNull(cursor);
        assertTrue("Missing columns", cursor.getColumnCount() == 10);
        assertTrue("No entries", cursor.getCount() > 0);
        cursor.close();
    }

    @Test
    public void testCursorToList() {
        db = cityDBHelper.getReadableDatabase();
        Cursor cursor = db.query(CityDB.CITIES_TABLE, null, null, null, null, null, null);
        List<City> cities = CityDB.getInstance().cursorToList(cursor);
        assertEquals(cities.size(), cursor.getCount());
        db.close();
    }

    @Test
    public void testSearchByName() {
        List<City> cities = CityDB.getInstance().searchByName("london");
        assertTrue(cities.size() == 6);
    }

    @Test
    public void testSearchByNameIsNotCaseSensitive() {
        List<City> cities = CityDB.getInstance().searchByName("lOnDoN");
        assertTrue(cities.size() == 6);
    }

    @Test
    public void testGetAll() {
        db = cityDBHelper.getReadableDatabase();
        Cursor cursor = db.query(CityDB.CITIES_TABLE, null, null, null, null, null, null);
        List<City> cities = CityDB.getInstance().getAll();
        assertEquals(cursor.getCount(), cities.size());
        cursor.close();
        db.close();
    }

    @Test
    public void testListIsNeverNull() {
        List<City> cities = CityDB.getInstance().searchByName("!no-such-entry!");
        assertNotNull(cities);
    }

    @Test
    public void testHandlesNullInSearch() {
        List<City> cities = CityDB.getInstance().searchByName(null);
        assertNotNull(cities);
    }

    @Test
    public void testHandlesEmptyStringInSearch() {
        List<City> cities = CityDB.getInstance().searchByName("");
        assertNotNull(cities);
    }

    @After
    public void tearDown() {

    }
}
