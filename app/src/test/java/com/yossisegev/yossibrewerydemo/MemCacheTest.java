package com.yossisegev.yossibrewerydemo;

import com.yossisegev.yossibrewerydemo.entities.BreweryLocation;
import com.yossisegev.yossibrewerydemo.model.cache.BreweryMemCache;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

/**
 * Created by Yossi Segev on 14/12/2016.
 */

public class MemCacheTest {

    private String TEST_ID = "test_id";
    @Test
    public void testAdd() {
        BreweryLocation breweryLocation = new BreweryLocation(TEST_ID);
        BreweryMemCache.getInstance().save(breweryLocation);
        assertNotNull(BreweryMemCache.getInstance().get(TEST_ID));
    }

    @Test
    public void testClear() {
        BreweryLocation breweryLocation = new BreweryLocation(TEST_ID);
        BreweryMemCache.getInstance().save(breweryLocation);
        BreweryMemCache.getInstance().clear();
        assertNull(BreweryMemCache.getInstance().get(TEST_ID));
    }

    @After
    public void tearDown() {
        BreweryMemCache.getInstance().clear();
    }
}
