package com.yossisegev.yossibrewerydemo.common;

/**
 * Created by Yossi Segev on 12/12/2016.
 */

public interface CallBack<R> {
    void onCompleted(R response);
    void onError(Exception e);
}
