package com.yossisegev.yossibrewerydemo.presentation;


import com.yossisegev.yossibrewerydemo.view.MvpView;

import java.lang.ref.WeakReference;




/**
 * Created by Yossi Segev on 12/12/16.
 */
public abstract class BasePresenter<V extends MvpView> {

    private WeakReference<V> viewWeakReference;

    public void attachView(V view) {
        viewWeakReference = new WeakReference<>(view);
        onViewAttached(viewWeakReference.get());
    }

    public void detachView() {
        if (viewWeakReference != null) {
            onViewDetaching(viewWeakReference.get());
            viewWeakReference.clear();
            viewWeakReference = null;
        }
    }

    protected V view() {
        if (viewWeakReference != null) {
            return viewWeakReference.get();
        }
        return null;
    }

    public boolean attached() {
        return viewWeakReference != null && viewWeakReference.get() != null;
    }

    protected void onViewAttached(V view) {
    }

    protected void onViewDetaching(V view) {
    }
}
