package com.yossisegev.yossibrewerydemo.presentation;

import com.google.android.gms.maps.model.Marker;

/**
 * Created by Yossi Segev on 12/12/2016.
 */

public interface LocationsPresenter {
    void findBreweriesByCity(String city);
    void onLocationSelected(String breweryId);
}
