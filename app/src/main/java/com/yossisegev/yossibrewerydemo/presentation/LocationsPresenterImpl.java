package com.yossisegev.yossibrewerydemo.presentation;

import android.text.TextUtils;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.yossisegev.yossibrewerydemo.common.CallBack;
import com.yossisegev.yossibrewerydemo.entities.BreweryLocation;
import com.yossisegev.yossibrewerydemo.model.api.BreweriesResponse;
import com.yossisegev.yossibrewerydemo.model.cache.BreweryMemCache;
import com.yossisegev.yossibrewerydemo.model.api.BreweryService;
import com.yossisegev.yossibrewerydemo.view.BreweriesLocationsView;

import java.util.List;

/**
 * Created by Yossi Segev on 12/12/2016.
 */

public class LocationsPresenterImpl extends BasePresenter<BreweriesLocationsView> implements LocationsPresenter {

    private BreweryService breweryService;
    private BreweryMemCache breweryMemCache;

    public LocationsPresenterImpl() {
        breweryService = new BreweryService();
        breweryMemCache = BreweryMemCache.getInstance();
    }

    @Override
    public void findBreweriesByCity(String city) {
        if (attached()) {
            view().showLoading(true);
        } else {
            return;
        }
        breweryService.getBreweriesByCity(city , new CallBack<BreweriesResponse>() {
            @Override
            public void onCompleted(BreweriesResponse response) {
                if (attached()) {
                    view().showLoading(false);
                    handleBreweriesResponse(response);
                }
            }

            @Override
            public void onError(Exception e) {
                if (attached()) {
                    view().showLoading(false);
                    view().onError(e);
                }
            }
        });
    }

    @Override
    public void onLocationSelected(String breweryId) {
        if (!TextUtils.isEmpty(breweryId)
                && breweryMemCache.get(breweryId) != null
                && attached()) {
            view().showBreweryLocationDetails(breweryMemCache.get(breweryId));
        }
    }

    private void handleBreweriesResponse(BreweriesResponse breweriesResponse) {
        List<BreweryLocation> locations = breweriesResponse.getLocations();
        breweryMemCache.clear();

        if (attached()) {
            view().clearLocations();
            view().showLocationsList(locations);
        } else {
            return;
        }

        LatLngBounds.Builder mapZoomBuilder = new LatLngBounds.Builder();
        for (BreweryLocation location : locations) {
            // Save to memCache;
            breweryMemCache.save(location);

            // Build map marker
            LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
            MarkerOptions markerOptions = new MarkerOptions();
            markerOptions.position(latLng);
            markerOptions.title(location.getBrewery().getName());
            if (attached()) {
                mapZoomBuilder.include(markerOptions.getPosition());
                view().markLocation(markerOptions, location.getId());
            } else {
                break;
            }
        }
        // Zoom map
        if (locations.size() > 0 && attached()) {
            view().zoomMap(CameraUpdateFactory.newLatLngBounds(mapZoomBuilder.build(), 0));
        }
    }

}
