package com.yossisegev.yossibrewerydemo.presentation;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;

import com.yossisegev.yossibrewerydemo.common.App;
import com.yossisegev.yossibrewerydemo.entities.City;
import com.yossisegev.yossibrewerydemo.model.db.CitySearchLoader;
import com.yossisegev.yossibrewerydemo.view.CitySearchView;

import java.util.List;

/**
 * Created by Yossi Segev on 13/12/2016.
 */

public class CitySearchPresenterImpl extends BasePresenter<CitySearchView> implements CitySearchPresenter, LoaderManager.LoaderCallbacks<List<City>> {

    private static final String ARGS_QUERY = "args:query";
    private LoaderManager loaderManager;

    public CitySearchPresenterImpl(@NonNull LoaderManager loaderManager) {
        this.loaderManager = loaderManager;
    }

    @Override
    protected void onViewAttached(CitySearchView view) {
        super.onViewAttached(view);
        loaderManager.initLoader(CitySearchLoader.ID, null, this);
    }

    @Override
    public void search(String searchQuery) {
        Bundle args = new Bundle();
        if (searchQuery == null) {
            searchQuery = "";
        }
        args.putString(ARGS_QUERY, searchQuery);
        loaderManager.restartLoader(CitySearchLoader.ID, args, this);
    }

    @Override
    public Loader<List<City>> onCreateLoader(int id, Bundle args) {
        String query = "";
        if (args != null && args.containsKey(ARGS_QUERY)) {
            query = args.getString(ARGS_QUERY);
        }
        return new CitySearchLoader(App.getContext(), query);
    }

    @Override
    public void onLoadFinished(Loader<List<City>> loader, List<City> data) {
        if (attached()) {
            view().onSearchResults(data);
        }
    }

    @Override
    protected void onViewDetaching(CitySearchView view) {
        super.onViewDetaching(view);
        loaderManager = null;
    }

    @Override
    public void onLoaderReset(Loader<List<City>> loader) {
    }
}
