package com.yossisegev.yossibrewerydemo.presentation;

/**
 * Created by Yossi Segev on 13/12/2016.
 */

public interface BreweryPresenter {
    void getBreweryById(String id);
}
