package com.yossisegev.yossibrewerydemo.presentation;

import android.text.TextUtils;

import com.yossisegev.yossibrewerydemo.model.cache.BreweryMemCache;
import com.yossisegev.yossibrewerydemo.view.BreweryView;

/**
 * Created by Yossi Segev on 13/12/2016.
 */

public class BreweryPresenterImpl extends BasePresenter<BreweryView> implements BreweryPresenter {

    @Override
    public void getBreweryById(String id) {
        if (attached() &&
                !TextUtils.isEmpty(id) &&
                BreweryMemCache.getInstance().get(id) != null) {

            view().showDetails(BreweryMemCache.getInstance().get(id));
        }
    }
}
