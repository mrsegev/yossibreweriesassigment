package com.yossisegev.yossibrewerydemo.presentation;

/**
 * Created by Yossi Segev on 12/12/2016.
 */

public interface BreweryDetailsPresenter  {
    void getBreweryById(String breweryId);
}
