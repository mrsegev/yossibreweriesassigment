package com.yossisegev.yossibrewerydemo.presentation;

import com.yossisegev.yossibrewerydemo.model.cache.BreweryMemCache;
import com.yossisegev.yossibrewerydemo.view.BreweryLocationDetailsView;

/**
 * Created by Yossi Segev on 12/12/2016.
 */

public class BreweryDetailsPresenterImpl extends BasePresenter<BreweryLocationDetailsView>
        implements BreweryDetailsPresenter {

    @Override
    public void getBreweryById(String breweryId) {
        if (BreweryMemCache.getInstance().get(breweryId) != null && attached()) {
            view().showBreweryLocationDetails(BreweryMemCache.getInstance().get(breweryId));
        }
    }
}
