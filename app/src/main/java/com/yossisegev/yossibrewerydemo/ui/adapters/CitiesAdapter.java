package com.yossisegev.yossibrewerydemo.ui.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.yossisegev.yossibrewerydemo.R;
import com.yossisegev.yossibrewerydemo.entities.City;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Yossi Segev on 13/12/2016.
 */

public class CitiesAdapter extends RecyclerView.Adapter<CitiesAdapter.CityViewHolder>{

    private List<City> cities;
    private OnCitySelectedListener onCitySelectedListener;

    public CitiesAdapter(OnCitySelectedListener onCitySelectedListener) {
        cities = new ArrayList<>();
        this.onCitySelectedListener = onCitySelectedListener;
    }

    public void setCities(List<City> cities) {
        if (cities == null) {
            this.cities.clear();
        } else {
            this.cities = cities;
        }
        notifyDataSetChanged();
    }

    @Override
    public CityViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cities_adapter_row, parent, false);
        return new CityViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CityViewHolder holder, int position) {
        final City city = cities.get(position);
        holder.name.setText(city.getName());
        holder.country.setText(city.getCountry());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (onCitySelectedListener != null) {
                    onCitySelectedListener.onCitySelected(city);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return cities.size();
    }

    public static class CityViewHolder extends RecyclerView.ViewHolder {
        TextView name;
        TextView country;

        public CityViewHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.cities_adapter_row_name);
            country = (TextView) itemView.findViewById(R.id.cities_adapter_row_country);
        }
    }

    public interface OnCitySelectedListener {
        void onCitySelected(City city);
    }
}
