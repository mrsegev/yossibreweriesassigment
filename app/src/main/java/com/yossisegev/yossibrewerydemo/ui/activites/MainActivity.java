package com.yossisegev.yossibrewerydemo.ui.activites;

import android.app.FragmentTransaction;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.yossisegev.yossibrewerydemo.R;
import com.yossisegev.yossibrewerydemo.entities.BreweryLocation;
import com.yossisegev.yossibrewerydemo.presentation.LocationsPresenterImpl;
import com.yossisegev.yossibrewerydemo.ui.adapters.LocationsAdapter;
import com.yossisegev.yossibrewerydemo.view.BreweriesLocationsView;

import java.util.List;

public class MainActivity extends AppCompatActivity implements BreweriesLocationsView, OnMapReadyCallback, LocationsAdapter.OnLocationSelectionListener {

    private LocationsPresenterImpl breweriesPresenter;
    private MapFragment mapFragment;
    private RecyclerView locationsRecyclerView;
    private TextView bottomSheetTitleText;
    private ProgressBar progressBar;
    private LinearLayout bottomSheet;
    private FloatingActionButton searchButton;
    private GoogleMap googleMap;
    private String currentSearchQuery = "";
    private static final int REQUEST_CITY = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        breweriesPresenter = new LocationsPresenterImpl();
        locationsRecyclerView = (RecyclerView) findViewById(R.id.activity_main_recycler_view);
        bottomSheetTitleText = (TextView) findViewById(R.id.activity_main_bottom_sheet_title);
        locationsRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        searchButton = (FloatingActionButton) findViewById(R.id.activity_main_search_button);
        progressBar = (ProgressBar) findViewById(R.id.activity_main_progress_bar);
        bottomSheet = (LinearLayout) findViewById(R.id.activity_main_bottom_sheet_layout);
        searchButton.setEnabled(false);
        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                 startActivityForResult(new Intent(MainActivity.this, CitySearchActivity.class), REQUEST_CITY);
            }
        });
        bottomSheet.setVisibility(View.GONE);
        initMap();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (breweriesPresenter != null) {
            breweriesPresenter.attachView(this);
        }
    }

    private void initMap() {
        mapFragment = MapFragment.newInstance();
        FragmentTransaction fragmentTransaction =
                getFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.activity_main_map_frame, mapFragment);
        fragmentTransaction.commit();
        mapFragment.getMapAsync(this);
    }

    @Override
    public void showLocationsList(List<BreweryLocation> locations) {
            String titleStr = String.format(getString(R.string.showing_x_breweries),
                    Integer.toString(locations.size()),
                    currentSearchQuery);

            bottomSheetTitleText.setText(titleStr);
            LocationsAdapter adapter = new LocationsAdapter(this, this);
            adapter.setLocations(locations);
            locationsRecyclerView.setAdapter(adapter);
            bottomSheet.setVisibility(View.VISIBLE);
    }

    @Override
    public void markLocation(MarkerOptions markerOptions, String breweryId) {
        Marker marker = googleMap.addMarker(markerOptions);
        marker.setTag(breweryId);
    }

    @Override
    public void zoomMap(CameraUpdate cameraUpdate) {
        if (googleMap != null) {
            googleMap.animateCamera(cameraUpdate);
        }
    }

    @Override
    public void clearLocations() {
        if (googleMap != null) {
            googleMap.clear();
        }
    }

    @Override
    public void showLoading(boolean show) {
        progressBar.setVisibility(show ? View.VISIBLE : View.GONE);
        searchButton.setVisibility(show ? View.GONE : View.VISIBLE);
    }

    @Override
    public void showBreweryLocationDetails(BreweryLocation location) {
        startActivity(BreweryLocationActivity.newIntent(this, location.getId()));
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        this.googleMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {
                breweriesPresenter.onLocationSelected((String) marker.getTag());
            }
        });
        searchButton.setEnabled(true);
        showLoading(false);
    }

    @Override
    public void onError(Exception e) {
        Toast.makeText(MainActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onLocationSelected(BreweryLocation location) {
        showBreweryLocationDetails(location);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CITY && resultCode == RESULT_OK && data != null) {
            currentSearchQuery = data.getStringExtra(CitySearchActivity.RESULT_CITY_NAME);
            breweriesPresenter.attachView(this);
            breweriesPresenter.findBreweriesByCity(currentSearchQuery);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (breweriesPresenter != null) {
            breweriesPresenter.detachView();
        }
    }
}
