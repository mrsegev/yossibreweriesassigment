package com.yossisegev.yossibrewerydemo.ui.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.yossisegev.yossibrewerydemo.R;
import com.yossisegev.yossibrewerydemo.entities.Brewery;
import com.yossisegev.yossibrewerydemo.entities.BreweryLocation;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Yossi Segev on 12/12/2016.
 */

public class LocationsAdapter extends RecyclerView.Adapter<LocationsAdapter.LocationViewHolder> {


    private Context context;
    private List<BreweryLocation> locations;
    private OnLocationSelectionListener onLocationSelectionListener;

    public LocationsAdapter(Context context, OnLocationSelectionListener onLocationSelectionListener) {
        this.context = context;
        locations = new ArrayList<>();
        this.onLocationSelectionListener = onLocationSelectionListener;
    }

    @Override
    public LocationViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.breweries_adapter_row, parent, false);
        return new LocationViewHolder(view);
    }
    public void setLocations(List<BreweryLocation> locations) {
        this.locations.addAll(locations);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return locations.size();
    }

    @Override
    public void onBindViewHolder(LocationViewHolder holder, int position) {
        final BreweryLocation location = locations.get(position);
        Brewery brewery = location.getBrewery();
        holder.name.setText(brewery.getName());
        holder.locationName.setText(location.getName());

        if (brewery.getSquareImage() != null) {
            Picasso.with(context)
                    .load(brewery.getSquareImage())
                    .into(holder.image);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (onLocationSelectionListener != null) {
                    onLocationSelectionListener.onLocationSelected(location);
                }
            }
        });
    }

    static class LocationViewHolder extends RecyclerView.ViewHolder {

        TextView name;
        TextView locationName;
        ImageView image;

        public LocationViewHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.locations_adapter_row_name);
            locationName = (TextView) itemView.findViewById(R.id.locations_adapter_row_location_name);
            image = (ImageView) itemView.findViewById(R.id.locations_adapter_row_image);
        }
    }

    public interface OnLocationSelectionListener {
        void onLocationSelected(BreweryLocation location);
    }
}
