package com.yossisegev.yossibrewerydemo.ui.activites;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.yossisegev.yossibrewerydemo.R;
import com.yossisegev.yossibrewerydemo.entities.BreweryLocation;
import com.yossisegev.yossibrewerydemo.presentation.BreweryPresenterImpl;
import com.yossisegev.yossibrewerydemo.view.BreweryView;

public class BreweryLocationActivity extends AppCompatActivity implements BreweryView {

    private static final String EXTRA_BREWERY_ID = "extra:brewery_id";

    private ImageView headerImage;
    private TextView descriptionText;
    private TextView addressText;
    private TextView websiteText;
    private TextView phoneText;

    public static Intent newIntent(Context context, @NonNull String breweryId) {
        Intent intent = new Intent(context, BreweryLocationActivity.class);
        intent.putExtra(EXTRA_BREWERY_ID, breweryId);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_brewery_location);
        headerImage = (ImageView) findViewById(R.id.activity_brewery_header_image);
        descriptionText = (TextView) findViewById(R.id.activity_brewery_description);
        addressText = (TextView) findViewById(R.id.activity_brewery_address);
        websiteText = (TextView) findViewById(R.id.activity_brewery_contact_website);
        phoneText = (TextView) findViewById(R.id.activity_brewery_contact_phone);
        BreweryPresenterImpl breweryPresenter = new BreweryPresenterImpl();
        breweryPresenter.attachView(this);

        String breweryId = getIntent().getStringExtra(EXTRA_BREWERY_ID);
        breweryPresenter.getBreweryById(breweryId);
    }

    @Override
    public void showDetails(BreweryLocation location) {

        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(location.getBrewery().getName());
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        if (!TextUtils.isEmpty(location.getBrewery().getHeaderImage())) {
            Picasso.with(this).load(location.getBrewery().getHeaderImage()).into(headerImage);
        } else {
            headerImage.setVisibility(View.GONE);
        }

        if (!TextUtils.isEmpty(location.getBrewery().getDescription())) {
            descriptionText.setText(location.getBrewery().getDescription());
        } else {
            descriptionText.setVisibility(View.GONE);
        }

        addressText.setText(buildAddressString(location));

        if (!TextUtils.isEmpty(location.getWebsite())) {
            websiteText.setText(location.getWebsite());
        } else {
            websiteText.setVisibility(View.GONE);
        }

        if (!TextUtils.isEmpty(location.getPhone())) {
            phoneText.setText(location.getPhone());
        } else {
            phoneText.setVisibility(View.GONE);
        }
    }

    private String buildAddressString(BreweryLocation location) {
        StringBuilder sb = new StringBuilder();
        sb.append(location.getStreetAddress());
        sb.append("\n");
        sb.append(location.getPostalCode());
        sb.append(" ");
        sb.append(location.getLocality());
        return sb.toString();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
