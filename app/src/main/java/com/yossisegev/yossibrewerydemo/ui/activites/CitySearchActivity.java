package com.yossisegev.yossibrewerydemo.ui.activites;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.yossisegev.yossibrewerydemo.R;
import com.yossisegev.yossibrewerydemo.entities.City;
import com.yossisegev.yossibrewerydemo.presentation.CitySearchPresenterImpl;
import com.yossisegev.yossibrewerydemo.ui.adapters.CitiesAdapter;
import com.yossisegev.yossibrewerydemo.view.CitySearchView;

import java.util.List;

public class CitySearchActivity extends AppCompatActivity implements CitySearchView, TextWatcher, CitiesAdapter.OnCitySelectedListener {

    public static final String RESULT_CITY_NAME = "result:city_name";
    private CitySearchPresenterImpl citySearchPresenter;
    private ProgressBar progressBar;
    private RecyclerView citiesRecyclerView;
    private EditText searchEditText;
    private CitiesAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_city_search);
        progressBar = (ProgressBar) findViewById(R.id.activity_city_search_progress);
        citiesRecyclerView = (RecyclerView) findViewById(R.id.activity_city_search_recycler_view);
        citiesRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new CitiesAdapter(this);
        citiesRecyclerView.setAdapter(adapter);
        searchEditText = (EditText) findViewById(R.id.activity_city_search_edit_text);
        searchEditText.addTextChangedListener(this);
        showLoading(true);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        citySearchPresenter = new CitySearchPresenterImpl(getSupportLoaderManager());
        citySearchPresenter.attachView(this);
    }

    @Override
    public void onCitySelected(City city) {
        Intent resultData = new Intent();
        resultData.putExtra(RESULT_CITY_NAME, city.getName());
        setResult(RESULT_OK, resultData);
        finish();
    }

    @Override
    public void afterTextChanged(Editable editable) {
       citySearchPresenter.search(editable.toString());
    }

    private void showLoading(boolean show) {
        citiesRecyclerView.setVisibility(show ? View.GONE : View.VISIBLE);
        searchEditText.setVisibility(show ? View.GONE : View.VISIBLE);
        progressBar.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onSearchResults(List<City> cities) {
        showLoading(false);
        adapter.setCities(cities);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (citySearchPresenter != null) {
            citySearchPresenter.detachView();
        }
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

}
