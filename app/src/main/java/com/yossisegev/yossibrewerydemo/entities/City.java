package com.yossisegev.yossibrewerydemo.entities;

/**
 * Created by Yossi on 12/13/16.
 */

public class City {

    private String name;
    private String country;

    public City(String name, String country) {
        this.name = name;
        this.country = country;
    }

    public String getCountry() {
        return country;
    }

    public String getName() {
        return name;
    }
}
