package com.yossisegev.yossibrewerydemo.entities;

/**
 * Created by Yossi Segev on 12/12/2016.
 */

public class BreweryImage {

    private String medium;
    private String squareMedium;

    public String getMedium() {
        return medium;
    }

    public String getSquareMedium() {
        return squareMedium;
    }
}
