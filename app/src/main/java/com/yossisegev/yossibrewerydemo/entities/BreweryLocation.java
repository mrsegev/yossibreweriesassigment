package com.yossisegev.yossibrewerydemo.entities;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Yossi Segev on 12/12/2016.
 */

public class BreweryLocation {

    private String id;
    private float latitude;
    private float longitude;
    private String name;
    private String streetAddress;
    private String locality;
    private String region;
    private String postalCode;
    private String phone;
    private String website;

    @SerializedName("brewery")
    private Brewery brewery;

    public BreweryLocation() {
    }

    public BreweryLocation(String id) {
        this.id = id;
    }

    public float getLatitude() {
        return latitude;
    }

    public float getLongitude() {
        return longitude;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getStreetAddress() {
        return streetAddress;
    }

    public String getLocality() {
        return locality;
    }

    public String getRegion() {
        return region;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public String getPhone() {
        return phone;
    }

    public Brewery getBrewery() {
        return brewery;
    }

    public String getWebsite() {
        return website;
    }


}
