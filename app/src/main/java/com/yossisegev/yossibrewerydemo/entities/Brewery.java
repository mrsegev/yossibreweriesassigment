package com.yossisegev.yossibrewerydemo.entities;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Yossi Segev on 12/12/2016.
 */

public class Brewery {

    private String id;
    private String name;
    private String description;
    private String website;

    @SerializedName("images")
    private BreweryImage image;

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getWebsite() {
        return website;
    }

    public String getSquareImage() {
        if (image != null) {
            return image.getSquareMedium();
        }
        return null;
    }

    public String getHeaderImage() {
        if (image != null) {
            return image.getMedium();
        }
        return null;
    }

}
