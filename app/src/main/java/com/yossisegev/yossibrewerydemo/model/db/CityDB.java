package com.yossisegev.yossibrewerydemo.model.db;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.yossisegev.yossibrewerydemo.common.App;
import com.yossisegev.yossibrewerydemo.entities.City;
import com.yossisegev.yossibrewerydemo.model.CitiesRepository;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Yossi on 12/13/16.
 */

public class CityDB implements CitiesRepository {

    public static final String CITIES_TABLE = "world_cities_table";
    public static final String ID = "_id";
    public static final String CITY = "city";
    public static final String CITY_ASCII = "city_ascii";
    public static final String LAT = "lat";
    public static final String LNG = "lng";
    public static final String POPULATION = "pop";
    public static final String COUNTRY = "country";
    public static final String PROVINCE = "province";
    public static final String ISO2 = "iso2";
    public static final String ISO3 = "iso3";

    private CityDBHelper dbHelper;

    private static class SingletonHolder {
        static CityDB INSTANCE = new CityDB();
    }

    private CityDB() {
        dbHelper = new CityDBHelper(App.getContext());
    }

    public static CityDB getInstance() {
        return SingletonHolder.INSTANCE;
    }

    @Override
    public List<City> getAll() {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor cursor = db.query(CityDB.CITIES_TABLE, null, null, null, null, null, null);
        List<City> cities = cursorToList(cursor);
        db.close();
        return cities;
    }

    @Override
    public List<City> searchByName(String searchQuery) {
        String query =
                "SELECT * FROM " + CITIES_TABLE
                        + " WHERE " + CITY + " LIKE \'%" + searchQuery + "%\' "
                        + " COLLATE NOCASE;";

        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        List<City> cities = cursorToList(cursor);
        db.close();
        return cities;
    }

    public List<City> cursorToList(final Cursor cursor) {
        List<City> cities = new ArrayList<>();
        while (cursor.moveToNext()) {
            int nameColumnInx = cursor.getColumnIndex(CITY);
            int countryColumnInx = cursor.getColumnIndex(COUNTRY);
            String name = cursor.getString(nameColumnInx);
            String country = cursor.getString(countryColumnInx);
            cities.add(new City(name, country));
        }
        cursor.close();
        return cities;
    }
}
