package com.yossisegev.yossibrewerydemo.model;

import com.yossisegev.yossibrewerydemo.entities.City;

import java.util.List;

/**
 * Created by Yossi on 12/13/16.
 */

public interface CitiesRepository {

    List<City> getAll();
    List<City> searchByName(String searchQuery);
}
