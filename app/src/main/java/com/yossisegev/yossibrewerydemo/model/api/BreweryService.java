package com.yossisegev.yossibrewerydemo.model.api;

import com.yossisegev.yossibrewerydemo.common.CallBack;
import com.yossisegev.yossibrewerydemo.model.BreweriesRepository;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Yossi Segev on 12/12/2016.
 */

public class BreweryService extends BaseService implements BreweriesRepository {


    private Api api;

    public BreweryService() {
        api = getRetrofit().create(Api.class);
    }

    @Override
    public void getBreweriesByCity(String city, final CallBack<BreweriesResponse> callBack) {
        Call<BreweriesResponse> call = api.get(city);
        executeCall(call, callBack);
    }

    private interface Api {
        @GET("v2/locations?key=" + Statics.BREWERY_DB_KEY)
        Call<BreweriesResponse> get(@Query("locality") String city);
    }

}
