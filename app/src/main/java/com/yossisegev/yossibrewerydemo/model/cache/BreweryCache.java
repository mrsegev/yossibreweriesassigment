package com.yossisegev.yossibrewerydemo.model.cache;

import com.yossisegev.yossibrewerydemo.entities.BreweryLocation;

/**
 * Created by Yossi Segev on 12/12/2016.
 */

public interface BreweryCache {

    BreweryLocation get(String id);

    void save(BreweryLocation brewery);

    void clear();
}
