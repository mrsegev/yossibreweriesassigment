package com.yossisegev.yossibrewerydemo.model.cache;

import com.yossisegev.yossibrewerydemo.entities.BreweryLocation;

import java.util.HashMap;

/**
 * Created by Yossi Segev on 12/12/2016.
 */

public class BreweryMemCache implements BreweryCache {

    private HashMap<String, BreweryLocation> cache;

    private static class SingletonHolder {
        static final BreweryMemCache INSTANCE = new BreweryMemCache();
    }

    public static BreweryMemCache getInstance() {
        return SingletonHolder.INSTANCE;
    }

    private BreweryMemCache() {
        cache = new HashMap<>();
    }

    @Override
    public BreweryLocation get(String id) {
        if (cache.containsKey(id)) {
            return cache.get(id);
        } else {
            return null;
        }
    }

    @Override
    public void save(BreweryLocation brewery) {
        if (brewery != null) {
            cache.put(brewery.getId(), brewery);
        }
    }

    @Override
    public void clear() {
        cache.clear();
    }
}
