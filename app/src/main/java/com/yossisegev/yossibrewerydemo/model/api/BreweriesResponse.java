package com.yossisegev.yossibrewerydemo.model.api;

import com.google.gson.annotations.SerializedName;
import com.yossisegev.yossibrewerydemo.entities.Brewery;
import com.yossisegev.yossibrewerydemo.entities.BreweryLocation;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * Created by Yossi Segev on 12/12/2016.
 */

public class BreweriesResponse {

    @SerializedName("data")
    List<BreweryLocation> locations;

    public List<BreweryLocation> getLocations() {
        if (locations == null) {
            locations = Collections.emptyList();
        }
        return locations;
    }
}
