package com.yossisegev.yossibrewerydemo.model.db;


import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;
import android.text.TextUtils;

import com.yossisegev.yossibrewerydemo.entities.City;

import java.util.List;

/**
 * Created by Yossi on 12/13/16.
 */

public class CitySearchLoader extends AsyncTaskLoader<List<City>> {
    public static final int ID = 100;
    private String newQuery;
    private List<City> cachedResults;
    private String cachedQuery = "";

    public CitySearchLoader(Context context, String searchQuery) {
        super(context);
        if (searchQuery == null) {
            this.newQuery = ""; // Prevent null value
        } else {
            this.newQuery = searchQuery;
        }
    }

    @Override
    protected void onStartLoading() {
        super.onStartLoading();
        if (cachedQuery.equals(newQuery) && cachedResults != null) {
            deliverResult(cachedResults);
        } else {
            forceLoad();
        }
    }

    @Override
    public List<City> loadInBackground() {
        if (TextUtils.isEmpty(newQuery)) {
            return CityDB.getInstance().getAll();
        } else {
            return CityDB.getInstance().searchByName(newQuery);
        }
    }

    @Override
    public void deliverResult(List<City> data) {
        cachedQuery = newQuery;
        cachedResults = data;
        super.deliverResult(data);
    }
}
