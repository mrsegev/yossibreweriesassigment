package com.yossisegev.yossibrewerydemo.model.api;

import com.yossisegev.yossibrewerydemo.common.CallBack;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Yossi Segev on 12/12/2016.
 */

class BaseService {

    private Retrofit retrofit;

    BaseService() {
        retrofit = new Retrofit.Builder()
                .client(new OkHttpClient())
                .baseUrl(Statics.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    protected Retrofit getRetrofit() {
        return retrofit;
    }

    protected <T> void executeCall(Call<T> call, final CallBack<T> callBack) {

        if (call == null || callBack == null) return;
        call.enqueue(new Callback<T>() {
            @Override
            public void onResponse(Call<T> call, Response<T> response) {
                if (response.isSuccessful() && response.body() != null) {
                    callBack.onCompleted(response.body());
                } else {
                    callBack.onError(new Exception("Failed to parse response"));
                }
            }

            @Override
            public void onFailure(Call<T> call, Throwable t) {
                callBack.onError(new Exception(t));
            }
        });
    }
}
