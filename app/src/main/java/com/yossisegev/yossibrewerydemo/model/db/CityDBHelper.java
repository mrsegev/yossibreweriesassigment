package com.yossisegev.yossibrewerydemo.model.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.yossisegev.yossibrewerydemo.R;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;


public class CityDBHelper extends SQLiteOpenHelper {

    private static final String TAG = "CityDBHelper";
    private Context context;
    private static final String DB_NAME = "city_db";

    public CityDBHelper(Context context) {
        super(context, DB_NAME, null, 1);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.beginTransaction();
        String CREATE_TBL = "CREATE TABLE IF NOT EXISTS " + CityDB.CITIES_TABLE
                + "("
                + CityDB.ID + " integer primary key autoincrement, "
                + CityDB.CITY + " text not null, "
                + CityDB.CITY_ASCII + " text not null,"
                + CityDB.LAT + " real not null,"
                + CityDB.LNG + " real not null,"
                + CityDB.POPULATION + " real not null,"
                + CityDB.COUNTRY + " text not null,"
                + CityDB.ISO2 + " text not null,"
                + CityDB.ISO3 + " text not null,"
                + CityDB.PROVINCE + " text not null"
                + ");";

        db.execSQL(CREATE_TBL);
        try {
            insertFromFile(context, R.raw.world_cities, db);
            db.setTransactionSuccessful();
            db.endTransaction();
        } catch (Exception e) {
            e.printStackTrace();
            db.endTransaction();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    private int insertFromFile(Context context, final int resourceId, final SQLiteDatabase db) throws IOException {
        int result = 0;
        InputStream insertsStream = context.getResources().openRawResource(resourceId);
        BufferedReader insertReader = new BufferedReader(new InputStreamReader(insertsStream));

        while (insertReader.ready()) {
            String insertStmt = insertReader.readLine();
            Log.d(TAG, "insertFromFile: " + insertStmt);
            db.execSQL(insertStmt);
            result++;
        }
        insertReader.close();

        return result;
    }
}
