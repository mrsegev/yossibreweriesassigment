package com.yossisegev.yossibrewerydemo.model.api;

/**
 * Created by Yossi Segev on 12/12/2016.
 */

public class Statics {

    static final String BREWERY_DB_KEY = "e0a7bda2eea137f7fd4d708be51786b4";
    static final String BASE_URL = "http://api.brewerydb.com/";
}
