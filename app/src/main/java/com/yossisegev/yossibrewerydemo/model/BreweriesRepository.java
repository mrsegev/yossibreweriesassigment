package com.yossisegev.yossibrewerydemo.model;

import com.yossisegev.yossibrewerydemo.common.CallBack;
import com.yossisegev.yossibrewerydemo.model.api.BreweriesResponse;

/**
 * Created by Yossi Segev on 12/12/2016.
 */

public interface BreweriesRepository {

    void getBreweriesByCity(String city, CallBack<BreweriesResponse> callBack);
}
