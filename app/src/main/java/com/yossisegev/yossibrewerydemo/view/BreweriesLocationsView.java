package com.yossisegev.yossibrewerydemo.view;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.model.MarkerOptions;
import com.yossisegev.yossibrewerydemo.entities.BreweryLocation;

import java.util.List;

/**
 * Created by Yossi Segev on 12/12/2016.
 */

public interface BreweriesLocationsView extends MvpView {

    void showLocationsList(List<BreweryLocation> locations);

    void markLocation(MarkerOptions markerOptions, String breweryId);

    void zoomMap(CameraUpdate cameraUpdate);

    void clearLocations();

    void showLoading(boolean show);

    void showBreweryLocationDetails(BreweryLocation brewery);

    void onError(Exception e);
}
