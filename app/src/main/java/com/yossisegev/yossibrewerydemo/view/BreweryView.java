package com.yossisegev.yossibrewerydemo.view;

import com.yossisegev.yossibrewerydemo.entities.BreweryLocation;

/**
 * Created by Yossi Segev on 13/12/2016.
 */

public interface BreweryView extends MvpView {
    void showDetails(BreweryLocation location);
}
