package com.yossisegev.yossibrewerydemo.view;

import com.yossisegev.yossibrewerydemo.entities.City;

import java.util.List;

/**
 * Created by Yossi Segev on 13/12/2016.
 */

public interface CitySearchView extends MvpView{
    void onSearchResults(List<City> cities);
}
