package com.yossisegev.yossibrewerydemo.view;

import com.yossisegev.yossibrewerydemo.entities.BreweryLocation;

/**
 * Created by Yossi Segev on 12/12/2016.
 */

public interface BreweryLocationDetailsView extends MvpView {
    void showBreweryLocationDetails(BreweryLocation location);
}
